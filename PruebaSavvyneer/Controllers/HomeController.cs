﻿using PruebaSavvyneer.Models;
using PruebaSavvyneer.Models.ProductProperties;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace PruebaSavvyneer.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            ResearchProduct secretProduct = new ResearchProduct();

            secretProduct.ID = 1000;
            secretProduct.Name = "R&D Product \"Savvy\"";
            secretProduct.ProductProperties = new List<IProductProperty>()
            {
                new Price()
                {
                    ID = 2000,
                    Key = "Price",
                    Value = "$800"
                },
                new Description()
                {
                    ID = 3000,
                    Key = "Description",
                    Value = "This product is looking to be one of Puerto Rico's largest exports. Get savvy today!"
                },
                new Manufacturer()
                {
                    ID = 4000,
                    Key = "Manufacturer",
                    Value = "Savvyneer LLC"
                },
                new Additional_Details()
                {
                    ID = 5000,
                    Key = "Details",
                    Value = "Available in Red, Blue and Yellow"
                }

            };

            return View(secretProduct);
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
    }
}