﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PruebaSavvyneer.Models
{
    public class ResearchProduct : IProduct
    {
        public int ID { get; set; }
        public string Name { get; set; }

        public List<IProductProperty> ProductProperties { get; set; }
    }
}