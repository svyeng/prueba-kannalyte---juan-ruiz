﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PruebaSavvyneer.Models
{
    public interface IProduct
    {
        int ID { get; set; }
        string Name { get; set; }

        List<IProductProperty> ProductProperties { get; set; }
    }
}
