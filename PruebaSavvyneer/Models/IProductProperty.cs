﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PruebaSavvyneer.Models
{
    public interface IProductProperty
    {
        int ID { get; set; }

        string Key { get; set; }
        string Value { get; set; }
    }
}
