﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PruebaSavvyneer.Models.ProductProperties
{
    public class Description : IProductProperty
    {
        public int ID { get; set; }
        public string Key { get; set; }
        public string Value { get; set; }
    }
}